# Plugin on request page to show/hide icons

## Information

	It is possible to only show certain quick menu icons on request page.

## Installation

	Please see your MSM documentation for information on how to install plugins.

## Dependence of MSM code

	- MenuController.addMenuItem
	- MenuController.onMenuItemClicked
	- MenuController._buildMenuFromMarkup

## Functions

### 1.0
	
	- Show and hide icons which are configured in plugin setting "ClassNameOfPrimaryIcons".
		Example of ClassNameOfPrimaryIcons: home;request hasSubMenu;worklist hasSubMenu;requestMessageAudit;manualNotification;convertRequest hasSubMenu

### 1.1
	- Menu Items based on user's role
	- Configuration is a json string like {'RoleId':'Classname, Classname'}
		Example of IndexOfPrimaryIcons: { '0':'0,1,2,3,4', '1':'0,1,2,6'}

## Compatible Versions

| Plugin   | MSM			|
|----------|----------------|
| 1.0	   | 14.10			|
| 1.1	   | 14.10,14.11	|