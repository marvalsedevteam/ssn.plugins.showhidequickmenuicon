<%@ WebHandler Language="C#" Class="GetGlobalSettings" %>

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json.Linq;
using MarvalSoftware.UI.WebUI.ServiceDesk.RFP.Plugins;

/// <summary>
///JiraHandler
/// </summary>
public class GetGlobalSettings : PluginHandler
{

    public override bool IsReusable
    {
        get { return false; }
    }

    public override void HandleRequest(HttpContext context)
    {
        var getGlobalSettings = context.Request.Params["GetGlobalSettings"] ?? string.Empty;
        if (!string.IsNullOrWhiteSpace(getGlobalSettings))
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(GetGlobalSetting());
        }
    }

    private string GetGlobalSetting ()
    {
        
		System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        string json = oSerializer.Serialize(GlobalSettings);
		
        return json;
    }
}