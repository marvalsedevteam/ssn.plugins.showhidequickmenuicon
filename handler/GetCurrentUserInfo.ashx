﻿<%@ WebHandler Language="C#" Class="GetCurrentUserInfo" %>

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Net.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MarvalSoftware.UI.WebUI.ServiceDesk.RFP.Plugins;


/// <summary>
/// ApiHandler
/// </summary>
public class GetCurrentUserInfo : IHttpHandler
{

    #region Properties

    /// <summary>
    /// Gets or sets whether or not this handler is reusable.
    /// </summary>
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    #endregion

    #region Methods

    public void ProcessRequest(HttpContext context)
    {
        string type = context.Request.QueryString["type"];

        // set the response content type
        context.Response.ContentType = "application/json";

        if(type=="ciid")
        {
            System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string json = oSerializer.Serialize(MarvalSoftware.Security.User.CurrentUser.ConfigurationItemId);
            context.Response.Write(json);
        }
        else if(type == "isInRole")
        {
            string roleId = context.Request.QueryString["roleId"];
            string role = new MarvalSoftware.Data.ServiceDesk.RoleBroker().Find(int.Parse(roleId)).Name;

            System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string json = oSerializer.Serialize(MarvalSoftware.Security.User.CurrentUser.IsInRole(role));
            context.Response.Write(json);
        }
        else if(type == "getRoles")
        {
            System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            MarvalSoftware.Security.User user = new MarvalSoftware.Data.ServiceDesk.UserBroker().Find(MarvalSoftware.Security.User.CurrentUser.Identifier);
            string json = oSerializer.Serialize(user.Roles);
            context.Response.Write(json);
        }
    }

    #endregion
}